﻿﻿﻿﻿# Excel 小游戏

#### 介绍
这是我刚学vba的时候做的游戏，这里部分游戏是我自己写的，部分游戏来源网络

#### 游戏目录

| 1    | 五子棋                  |
| ---- | ----------------------- |
| 2    | 三国杀                  |
| 3    | 俄罗斯方块-界面版       |
| 4    | 俄罗斯方块-窗体版       |
| 5    | 俄罗斯方块              |
| 6    | 卡牌video poker         |
| 7    | 图灵游戏                |
| 8    | 坦克                    |
| 9    | 娱票儿票房分析3         |
| 10   | 扫雷                    |
| 11   | 数独3                   |
| 12   | 斗地主                  |
| 13   | 游戏sliding tile puzzle |
| 14   | 窗体俄罗斯方块          |
| 15   | 翻卡牌                  |
| 16   | 聚光灯效果              |
| 17   | 象棋2                   |
| 18   | 贪吃蛇                  |
| 19   | 躲砖块游戏              |
| 20   | 迷宫                    |
| 21   | version-1.3-arena汉化版 |
| 22   | 2048                    |
| 23   | Cellivization v1        |
| 24   | EXCELB1                 |
| 25   | EXCEL三国杀C1           |


#### 安装教程

 启动宏就可以使用：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200818182403244.png#pic_center)



#### 使用说明

1. 这是Excel版的斗地主
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200818182414444.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NDI0MjIz,size_16,color_FFFFFF,t_70#pic_center)



2. 这是掉砖块游戏
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200818182447657.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NDI0MjIz,size_16,color_FFFFFF,t_70#pic_center)



3. 俄罗斯方块
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200818182456545.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NDI0MjIz,size_16,color_FFFFFF,t_70#pic_center)

  

  #### 下载地址：

  关注公众号《软设开发》领取更多实用工具，回复关键字：**小游戏**

  [原文链接](https://mp.weixin.qq.com/s/u3G3Q5idkdI--8qi8QJlLA)
![输入图片说明](blank.png)

